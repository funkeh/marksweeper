-- chikun :: 2015
-- Marksweeper main loop


require "src/onClick"

dead = true

firstStartup = true


function love.load()

    love.graphics.setBackgroundColor(128, 128, 128)

    tiles = { }
    tileCovers = { }

    bombs = 20
    bombsPlanted = 0

    -- Create tiles
    for i=1, 15 do

        tiles[i] = { }
        tileCovers[i] = { }

        for k=1, 15 do

            tiles[i][k] = 0

            tileCovers[i][k] = true

        end
    end


    -- Load graphics
    tileGFX = {
        love.graphics.newImage('gfx/block.png'),
        love.graphics.newImage('gfx/empty.png')
    }

    markGFX = {
        dead    = love.graphics.newImage('gfx/markDead.png'),
        happy   = love.graphics.newImage('gfx/markHappy.png'),
        unhappy = love.graphics.newImage('gfx/markUnhappy.png')
    }

    markBox = {
        x = love.graphics.getWidth() / 2 - 50,
        y = 175 / 2 - 50,
        w = 100,
        h = 100
    }

    tileScale = 30 / 100


end


function love.update(dt)

    if dead then

        if not deadTimer then

            deadTimer = 0

        else

            deadTimer = math.min(deadTimer + dt, 1)

            if deadTimer == 1 then

                deadTimer = nil

                love.load()

                dead = false

                firstStartup = false

            end
        end

    end

end


function love.draw()

    love.graphics.setColor(255, 255, 255)


    -- Draw button
    love.graphics.draw(tileGFX[1], markBox.x, markBox.y)
    local markPic = markGFX.happy
    if dead then
        markPic = markGFX.dead
    elseif love.mouse.isDown('l') then
        markPic = markGFX.unhappy
    end
    love.graphics.draw(markPic, markBox.x + 10, markBox.y + 10)


    -- Draw tiles
    for x, value in ipairs(tiles) do

        for y, tile in ipairs(value) do

            local newTile = {
                x = 15 + (x - 1) * 30,
                y = 175 + (y - 1) * 30,
                w = 30,
                h = 30
            }

            if not tileCovers[x][y] then

                love.graphics.draw(tileGFX[2], newTile.x, newTile.y, 0, tileScale)
            end

            if tileCovers[x][y] then

                love.graphics.draw(tileGFX[1], newTile.x, newTile.y, 0, tileScale)
            end

        end
    end

    if firstStartup then

        love.graphics.setColor(0, 0, 0)

        love.graphics.rectangle('fill', 0, 0, 480, 640)

    end

end


function love.mousepressed(x, y, mb)

    if mb == 'l' and not dead then

        onClick(x, y)

    end
end




-- Checks if two rectangle objects overlap
function math.overlap(obj1, obj2)

    return (obj1.x < obj2.x + obj2.w and
            obj2.x < obj1.x + obj1.w and
            obj1.y < obj2.y + obj2.h and
            obj2.y < obj1.y + obj1.h)

end
