-- chikun :: 2015
-- What happens when you click?


function onClick(x, y)

    local cursor = {
        x = x,
        y = y,
        w = 1,
        h = 1
    }

    if math.overlap(cursor, markBox) then

        love.load()

        return 0

    end

    -- Check tiles
    for x, value in ipairs(tiles) do

        for y, tile in ipairs(value) do

            local newTile = {
                x = 15 + (x - 1) * 30,
                y = 175 + (y - 1) * 30,
                w = 30,
                h = 30
            }

            if math.overlap(newTile, cursor) then

                dead = true

            end

        end
    end

end
